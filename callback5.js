const getBoardDetailsById = require('./callback1');
const getListsByBoard = require('./callback2');
const getCardsByListId = require('./callback3');

const thanosBoardListAndMindCards = () => {
    const MINTIMELIMIT = 2 * 1000;// 2 Seconds

    setTimeout(() => {
        const ThanosBoardId = 'mcu453ed';
        getBoardDetailsById(ThanosBoardId, (error, boardDetails) => {
            if (error) {
                console.error(`ERROR: Getting Data`, error);
            } else {
                console.log(`Thanos Board Details`);
                console.log(boardDetails);

                getListsByBoard(ThanosBoardId, (error, lists) => {
                    if (error) {
                        console.log(`ERROR:`, error);
                    } else {
                        console.log(`All lists of Thanos Board`);
                        console.log(lists);

                        listNeeded = ['mind', 'space'];

                        for (let list of lists) {
                            const listName = list.name.toLowerCase();

                            if (listNeeded.includes(listName)) {
                                getCardsByListId(list.id, (error, cards) => {
                                    if (error) {
                                        console.error(`ERROR`, error);
                                    } else {
                                        console.log(`Cards In list ${list.name}`);
                                        console.log(cards);
                                    }
                                });

                            }
                        }
                    }
                });
            }
        });

    }, MINTIMELIMIT);
}

module.exports = thanosBoardListAndMindCards;