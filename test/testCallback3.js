const callback3 = require('../callback3');

const soulListId = 'azxs123';

callback3(soulListId, (error, data) => {
    if (error) {
        console.error(`ERROR: Getting Data`, error);
    } else {
        console.log(`SUCCESS: Getting Data`);
        console.log(data);
    }
});