const callback2 = require('../callback2');

const ThanosBoardId = 'mcu453ed';

callback2(ThanosBoardId, (error, data) => {
    if (error) {
        console.error(`ERROR: Getting Data`, error);
    } else {
        console.log(`SUCCESS: Getting Data`);
        console.log(data);
    }
});