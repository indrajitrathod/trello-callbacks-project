const callback1 = require('../callback1');

const ThanosBoardId = 'mcu453ed';

callback1(ThanosBoardId, (error, data) => {
    if (error) {
        console.error(`ERROR: Getting Data`, error);
    } else {
        console.log(`SUCCESS: Getting Data`);
        console.log(data);
    }
});