const boards = require('./boards.json');

const getBoardDetailsById = (id, callback) => {

    if (id === undefined || callback === undefined || typeof id !== 'string' || typeof callback !== 'function') {
        const error = 'Function Parameter should be ( boardId<string>, callback <callback-function> )';
        callback(error);
        return;
    }

    const MINTIMELIMIT = 2 * 1000;// 2 Seconds

    setTimeout(() => {
        const board = boards.filter((board) => {
            return board.id === id;
        });

        if (board.length === 0) {
            const error = 'BOARD NOT FOUND';
            callback(error);
        } else {
            callback(null, board[0]);
        }
    }, MINTIMELIMIT);
}

module.exports = getBoardDetailsById;