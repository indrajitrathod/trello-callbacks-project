const lists = require('./lists.json');

const getListsByBoardId = (id, callback) => {

    if (id === undefined || callback === undefined || typeof id !== 'string' || typeof callback !== 'function') {
        const error = 'Function Parameter should be ( boardId<string>, callback <callback-function> )';
        callback(error);
        return;
    }

    const MINTIMELIMIT = 2 * 1000;// 2 Seconds

    setTimeout(() => {
        if (!lists[id]) {
            const error = `LIST NOT FOUND: of BoardId- ${id}`;
            callback(error);
        } else {
            callback(null, lists[id]);
        }
    }, MINTIMELIMIT);


}

module.exports = getListsByBoardId;