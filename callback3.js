const cards = require('./cards.json');

const getCardsByListId = (id, callback) => {

    if (id === undefined || callback === undefined || typeof id !== 'string' || typeof callback !== 'function') {
        const error = 'Function Parameter should be ( boardId<string>, callback <callback-function> )';
        callback(error);
        return;
    }

    const MINTIMELIMIT = 2 * 1000;// 2 Seconds

    setTimeout(() => {
        if (!cards[id]) {
            const error = `CARDS NOT FOUND:  with List id: ${id}`;
            callback(error);
        } else {
            callback(null, cards[id]);
        }
    }, MINTIMELIMIT);

}

module.exports = getCardsByListId;